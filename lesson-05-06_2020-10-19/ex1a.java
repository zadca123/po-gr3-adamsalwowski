import java.util.Arrays;
import java.util.Random;

public class ex1a{
    public static void main(String[] args){
        Random rand_num = new Random();
        int n = rand_num.nextInt(101)+1; // od 1 do 100
        int[] array = new int[n];
        int min=-999;
        int max=999;
        for(int i=0; i<array.length; i++){
            array[i] = rand_num.nextInt(max-min)+min;
            System.out.print(array[i] + ", ");
        }

        int even = 0, odd = 0;
        for(int num: array){
            if(num % 2 == 0)
                even++;
            // if(!(num % 2 == 0))
            else
                odd++;
        }
        System.out.println("Number of even integers:\t" + even);
        System.out.println("Number of not even integers:\t" + odd);
    }
}
