import java.util.Arrays;
import java.util.Random;

public class ex2f{
    public static void main(String[] args){
        int len = getRandomNum(1,101);
        int[] array = new int[len];
        generate(array, len, -999, 999);

        signum(array);
    }
    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generate(int[] tab, int len, int min, int max){
        for(int i=0; i<tab.length;i++){
                tab[i] = getRandomNum(min,max);
                System.out.print(tab[i] + "\t");
            }
        System.out.println();
    }
    public static void signum(int[] tab){
        for(int i=0;i<tab.length;i++){
            if(tab[i]>0)
                tab[i] = 1;
            else if(tab[i]<0)
                tab[i] = -1;
            System.out.print(tab[i] +"\t");
        }
    }
}
