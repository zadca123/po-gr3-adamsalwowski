import java.util.Arrays;
import java.util.Random;

public class ex2g{
    public static void main(String[] args){
        int len = getRandomNum(1,101);
        int[] array = new int[len];
        generate(array, len, -999, 999);

        int lewy=getRandomNum(1,len);
        int prawy=getRandomNum(1,len);
        odwrocFragment(array, lewy, prawy);
    }
    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generate(int[] tab, int n, int min, int max){
        for(int i=0; i<n; i++){
            tab[i] = getRandomNum(min,max);
            System.out.print(tab[i] + ", ");
        }
        System.out.println();
    }
    public static void odwrocFragment(int[] array,int lewy, int prawy){
        int temp;
        if(lewy<prawy){
            for(int i=0; i<(prawy-lewy)/2; i++){
                temp=array[lewy+i];
                array[lewy+i]=array[prawy-i-1];
                array[prawy-i-1] = temp;
            }
            for(int i=0; i<array.length; i++)
                System.out.print(array[i] +", ");
        }
        else
            System.out.println("Lewy= "+ lewy +" is not smaller than "+ prawy +" =Prawy");
    }
}
