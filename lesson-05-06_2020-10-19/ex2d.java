import java.util.Arrays;
import java.util.Random;

public class ex2d{
    public static void main(String[] args){
        int len = getRandomNum(1,101);
        int[] array = new int[len];
        generate(array, len, -999, 999);


        System.out.println("Sum of positive elements in array: " + sumaDodatnich(array));
        System.out.println("Sum of negative elements in array: " + sumaUjemnych(array));
    }
    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generate(int[] tab, int len, int min, int max){
        for(int i=0; i<tab.length;i++){
                tab[i] = getRandomNum(min,max);
                System.out.print(tab[i] + ", ");
            }
        System.out.println();
    }
    public static int sumaDodatnich(int[] tab){
        int result=0;
        for(int i=0; i<tab.length;i++)
            if(tab[i]>0)
                result += tab[i];
        return result;
    }
    public static int sumaUjemnych(int[] tab){
        int result=0;
        for(int i=0; i<tab.length;i++)
            if(tab[i]<0)
                result += tab[i];
        return result;
    }
}
