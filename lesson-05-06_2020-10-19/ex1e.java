import java.util.Arrays;
import java.util.Random;

public class ex1e{
    public static void main(String[] args){
        Random rand_num = new Random();
        int n = rand_num.nextInt(101)+1; // od 1 do 100
        int[] array = new int[n];
        int min=-999;
        int max=999;
        for(int i=0; i<array.length; i++){
            array[i] = rand_num.nextInt(max-min)+min;
            System.out.print(array[i] + ", ");
        }
        System.out.println();

        int result = 0, temp=0;
        for(int num: array){
            if(num > 0){
                temp++;
                if(temp>result)
                    result = temp;
            }
            else
                temp = 0;
        }
        System.out.println("Max lenght of positive numbers: "+ result);
    }
}
