import java.util.Arrays;
import java.util.Random;

public class ex2e{
    public static void main(String[] args){
        int len = getRandomNum(1,101);
        int[] array = new int[len];
        generate(array, len, -999, 999);

        System.out.println("Max lenght of positive numbers: "+ dlugoscMaksymalnegoCiaguDodatnich(array));
    }
    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generate(int[] tab, int len, int min, int max){
        for(int i=0; i<tab.length;i++){
                tab[i] = getRandomNum(min,max);
                System.out.print(tab[i] + ", ");
            }
        System.out.println();
    }
    public static int ileDodatnich(int[] tab){
        int result=0;
        for(int i=0; i<tab.length;i++)
            if(tab[i] % 2 == 0)
                result++;
        return result;
    }
    public static int ileUjemnych(int[] tab){
        int result=0;
        for(int i=0; i<tab.length;i++)
            if(tab[i] % 2 != 0)
                result++;
        return result;
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] tab){
        int result = 0, temp=0;
        for(int i=0; i<tab.length; i++){
            if(tab[i]> 0){
                temp++;
                if(temp>result)
                    result = temp;
            }
            else
                temp = 0;
        }
        return result;
    }
}
