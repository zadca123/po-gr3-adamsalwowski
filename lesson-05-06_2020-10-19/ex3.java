import java.util.Arrays;
import java.util.Random;

public class ex3{
    public static void main(String[] args){
        int m = getRandomNum(1,11);
        int n = getRandomNum(1,11);
        int k = getRandomNum(1,11);

        matrix_stuff(m,n,k);
    }
    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generate(int[] tab, int n, int min, int max){
        for(int i=0; i<n; i++){
            tab[i] = getRandomNum(min,max);
            System.out.print(tab[i] + ", ");
        }
        System.out.println();
    }
    // public static int matrix_create[][](int m, int n){
    //     int[][] result = int new[m][n];
    //     for(int i=0;i<m;i++)
    //         for(int j=0;j<n;j++)
    //             result[i][j] = getRandomNum(1,11);
    //     return result[][];
    // }
    public static void matrix_print(int[][] result, int m, int n){
        for(int i=0;i<m;i++){
            System.out.print("[ ");
            for(int j=0;j<n;j++)
                System.out.print(result[i][j] +"\t");
            System.out.print("]\n");
        }
    }
    public static void matrix_stuff(int m, int n, int k){
        int[][] a = new int [m][n];
        int[][] b = new int [n][k];
        int[][] c = new int [m][k];

        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                a[i][j]= getRandomNum(1,11);
            }
        }
        for(int i=0; i<n; i++){
            for(int j=0;j<k;j++){
                b[i][j]= getRandomNum(1,11);
            }
        }
        for(int i=0; i<m; i++){
            for(int j=0;j<k;j++)
                for(int l=0;l<n;l++)
                    c[i][j] += a[i][l] * b[l][j];
        }

        matrix_print(a,m,n);
        System.out.println("^^^^ Matrix A ^^^^");
        matrix_print(b,n,k);
        System.out.println("^^^^ Matrix B ^^^^");
        matrix_print(c,m,k);
        System.out.println("^^^^ Matrix C ^^^^");
    }
}
