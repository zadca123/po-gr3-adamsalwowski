import java.util.Arrays;
import java.util.Random;

public class ex2a{
    public static void main(String[] args){
        int len = getRandomNum(1,101);
        int[] array = new int[len];
        generate(array, len, -999, 999);

        System.out.println("Numbers of odd elements: " + NumOdd(array));
        System.out.println("Numbers of even elements: " + NumEven(array));
    }
    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generate(int[] tab, int n, int min, int max){
        for(int i=0; i<n; i++){
                tab[i] = getRandomNum(min,max);
                System.out.print(tab[i] + ", ");
            }
        System.out.println();
    }
    public static int NumOdd(int[] tab){
        int result = 0;
        for(int i=0; i<tab.length; i++)
            if(!(tab[i] % 2 == 0))
                result++;
        return result;
    }
    public static int NumEven(int[] tab){
        int result = 0;
        for(int i=0; i<tab.length; i++)
            if(tab[i] % 2 == 0)
                result++;
        return result;
    }
}
