import java.util.Arrays;
import java.util.Random;

public class ex1d{
    public static void main(String[] args){
        Random rand_num = new Random();
        int n = rand_num.nextInt(101)+1; // od 1 do 100
        int[] array = new int[n];
        int min=-999;
        int max=999;
        for(int i=0; i<array.length; i++){
            array[i] = rand_num.nextInt(max-min)+min;
            System.out.print(array[i] + ", ");
        }
        System.out.println();

        int positiv_sum = 0, negativ_sum = 0;
        for(int num: array){
            if(num > 0){
                positiv_sum += num;
            }
            else
                negativ_sum += num;
        }
        System.out.println("Positive sum: "+ positiv_sum);
        System.out.println("Negative sum: "+ negativ_sum);
    }
}
