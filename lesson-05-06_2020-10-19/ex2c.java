import java.util.Arrays;
import java.util.Random;

public class ex2c{
    public static void main(String[] args){
        int len = getRandomNum(1,101);
        int[] array = new int[len];
        generate(array, len, -999, 999);

        System.out.println("Number of max numbers in array is: "+ ileMaksymalnych(array));
    }
    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generate(int[] tab, int len, int min, int max){
        for(int i=0; i<tab.length;i++){
                tab[i] = getRandomNum(min,max);
                System.out.print(tab[i] + ", ");
            }
        System.out.println();
    }
    public static int ileMaksymalnych(int[] tab){
        int result=0,max=tab[0];
        for(int i=0; i<tab.length;i++)
            if(tab[i]>max){
                max = tab[i];
                result = 1;
            }
            else if(tab[i]== max)
                result++;
        System.out.println("Max number: "+ max);
        return result;
    }
}
