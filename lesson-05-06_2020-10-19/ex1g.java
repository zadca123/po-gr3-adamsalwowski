import java.util.Arrays;
import java.util.Random;

public class ex1g{
    public static void main(String[] args){
        Random rand_num = new Random();
        int n = rand_num.nextInt(101)+1; // od 1 do 100
        int[] array = new int[n];
        int min=-999;
        int max=999;
        for(int i=0; i<array.length; i++){
            array[i] = rand_num.nextInt(max-min)+min;
            System.out.print(array[i] + ", ");
        }
        System.out.println();

        int lewy =rand_num.nextInt(n)+1;
        int prawy=rand_num.nextInt(n)+1;
        int temp;
        if(lewy<prawy){
            for(int i=0; i<(prawy-lewy)/2; i++){
                temp=array[lewy+i];
                array[lewy+i]=array[prawy-i-1];
                array[prawy-i-1] = temp;
            }
            System.out.println("\n~~~Array with flipped part below~~~");
            for(int i=0; i<array.length; i++)
                System.out.print(array[i] +", ");
        }
        else
            System.out.println("Lewy="+ lewy +" is not smaller than "+ prawy +"=Prawy");
    }
}
