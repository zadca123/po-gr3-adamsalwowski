import java.util.Arrays;
import java.util.Random;

public class ex1b{
    public static void main(String[] args){
        Random rand_num = new Random();
        int n = rand_num.nextInt(101)+1; // od 1 do 100
        int[] array = new int[n];
        int min=-999;
        int max=999;
        for(int i=0; i<array.length; i++){
            array[i] = rand_num.nextInt(max-min)+min;
            System.out.print(array[i] + ", ");
        }
        System.out.println();
        int positive = 0, negative = 0, zeroes = 0;
        for(int num: array){
            if(num>0)
                positive++;
            else if(num<0)
                negative++;
            else
                zeroes++;
        }
        System.out.println("Number of positive numbers: "+ positive);
        System.out.println("Number of negative numbers: "+ negative);
        System.out.println("Number of zeroes : "+ zeroes);
    }
}
