import java.lang.StringBuffer;
import java.util.Arrays;
import java.io.File;
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.util.Scanner;

public class ex2{
    public static int search(File file, char found){
        int result = 0;
        try {
            File myObj = file;
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
                char[] data_char = data.toCharArray();
                for (int i = 0; i < data_char.length; i++)
                    if(data_char[i]==found)
                        result++;
            }
            myReader.close();
        }
        catch (FileNotFoundException error) {
            System.out.println("An error occurred.");
            error.printStackTrace();
        }
        return result;
    }
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a character to search: ");
        char found = input.next().charAt(0);
        File file= new File("ex2.txt");

        System.out.printf("%d\n", search(file, found));
    }
}
