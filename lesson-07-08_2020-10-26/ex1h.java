import java.lang.StringBuffer;
import java.util.Arrays;

public class ex1h{
    public static void main(String[] args){
        String s="pppxxxyyyeee";
        String sep = "[]";
        System.out.println(nice(s,sep));
    }
    public static String nice(String str, String sep){
        int temp = 0;
        char[] str_char = str.toCharArray();
        StringBuffer result = new StringBuffer(str.length()+str.length()/3);
        for (int i=0; i < str.length(); i++) {
            if((str.length()-i)%3==0)
                result.append(sep);
            result.append(str_char[i]);
        }
        return result.toString();
    }
}
