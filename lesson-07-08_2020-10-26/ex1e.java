import java.util.Arrays;
import java.util.Scanner;

public class ex1e{
    public static void main(String[] args){
        String str= "ddweddd";
        String subStr= "dd";
        int[] tab = where(str, subStr);
        System.out.print("[");
        for (int i = 0; i < tab.length; i++)
                System.out.printf("%d, ",tab[i]);
        System.out.print("]");
    }
    public static int[] where(String str, String subStr){
        char[] str_char= str.toCharArray();
        char[] subStr_char= subStr.toCharArray();
        int[] result = new int[countSubStr(str,subStr)];
        result[0] = 0;
        int len = str_char.length - subStr_char.length + 1;
        int k = 0;
        for (int i = 0; i < len; i++) {
            int count = 0;
            // System.out.println("Count=0");
            for (int j = 0; j < subStr_char.length; j++) {
                if(subStr_char[j] == str_char[i+j]){
                    count++;
                    // System.out.println("Count++");
                    if(count == subStr_char.length){
                        result[k] = i;
                        // System.out.println("Result["+ k +"]= "+ result[k]);
                        k++;
                    }
                }
            }
        }
        return result;
    }
    public static int countSubStr(String str, String subStr){
        char[] str_char= str.toCharArray();
        char[] subStr_char= subStr.toCharArray();
        int result = 0;
        int len = str_char.length - subStr_char.length + 1;
        for (int i = 0; i < len; i++) {
            int count = 0;
            for (int j = 0; j < subStr_char.length; j++) {
                if(subStr_char[j] == str_char[i+j]){
                    count++;
                    if(count == subStr_char.length){
                        result++;
                    }
                }

            }
        }
        return result;
    }
}
