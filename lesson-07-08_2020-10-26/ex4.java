import java.math.BigInteger;

public class ex4{
    public static BigInteger board(int n){
        BigInteger result = new BigInteger("0");
        BigInteger temp = new BigInteger("1");
        for (int i = 0; i < n*n; i++){
            result = result.add(temp);
            temp = temp.multiply(BigInteger.TWO);
        }
        return result;
    }

    public static void main(String[] args){
        int n = 10;
        System.out.printf("%d\n", board(n));
    }
}
