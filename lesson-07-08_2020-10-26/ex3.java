import java.lang.StringBuffer;
import java.util.Arrays;
import java.io.File;
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.util.Scanner;

public class ex3{
    public static int search(File file, String found){
        int result = 0;
        try {
            File myObj = file;
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);

                char[] data_char = data.toCharArray();
                char[] found_char = found.toCharArray();
                int counter = 0;
                for (int i = 0; i < data_char.length - found_char.length+1; i++){ // without +1 the last character will be skipped
                    counter = 1; // counter = 1 because j=1 from the start
                    if(data_char[i]==found_char[0])
                        for (int j = 1; j < found_char.length; j++) {
                            if(found_char[j] == data_char[i+j])
                                counter++;
                            if(counter == found_char.length)
                                result++;
                        }
                }
            }
            myReader.close();
        }
        catch (FileNotFoundException error) {
            System.out.println("An error occurred.");
            error.printStackTrace();
        }
        return result;
    }
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a character to search: ");
        String found = input.next();
        File file= new File("ex2.txt");

        System.out.printf("%d\n", search(file, found));
    }
}
