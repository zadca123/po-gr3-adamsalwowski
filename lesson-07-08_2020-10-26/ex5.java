import java.math.BigDecimal;

public class ex5{
    public static BigDecimal capitalism(int n, BigDecimal k, BigDecimal p){
        BigDecimal result = k;
        BigDecimal temp;
        for (int i = 0; i < n; i++){
            temp = result;
            temp = temp.multiply(p);
            result = result.add(temp);
        }
        return result;
    }

    public static void main(String[] args){
        int n = 2;
        BigDecimal k = BigDecimal.valueOf(200000);
        BigDecimal p = BigDecimal.valueOf(0.009);
        System.out.printf("%.3f\n", capitalism(n,k,p));
    }
}
