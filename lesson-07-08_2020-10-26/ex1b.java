import java.util.Arrays;

public class ex1b{
    public static void main(String[] args){
        String str= "Sersesser";
        String subStr= "ser";
        System.out.println(countSubStr(str, subStr));
    }
    public static int countSubStr(String str, String subStr){
        char[] str_char= str.toCharArray();
        char[] subStr_char= subStr.toCharArray();
        int result = 0;
        int len = str_char.length - subStr_char.length + 1;
        for (int i = 0; i < len; i++) {
            int count = 0;
            // System.out.println("Count=0");
            for (int j = 0; j < subStr_char.length; j++) {
                if(subStr_char[j] == str_char[i+j]){
                    count++;
                    // System.out.println("Count++");
                    if(count == subStr_char.length){
                        result++;
                        // System.out.println("Result++");
                    }
                }

            }
        }
        return result;
    }
}
