import java.lang.StringBuffer;
import java.util.Arrays;

public class ex1f{
    public static void main(String[] args){
        String s="czEsC";
        System.out.println(change(s));
    }
    public static String change(String str){
        char[] Chararray = new char[str.length()];
        StringBuffer s = new StringBuffer(str.length());
        for(int i=0; i<str.length(); i++)
           Chararray[i] = str.charAt(i);

        char[] NChararray = new char[str.length()];
        for(int i=0; i<str.length(); i++){
            if(Character.isUpperCase(Chararray[i]))
                NChararray[i] = Character.toLowerCase(Chararray[i]);
            else
                NChararray[i] = Character.toUpperCase(Chararray[i]);
        }

        for(int i=0; i<NChararray.length; i++)
            s.append(NChararray[i]);
        return s.toString();
    }
}
