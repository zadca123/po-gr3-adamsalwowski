// import java.util.Arrays;

public class ex1c{
    public static void main(String[] args){
        String char_array= "middle";
        System.out.println(middle(char_array));
    }
    public static String middle(String str){
        String m = "";
        if(str.length()<=1)
            return "The string does not have middle";
       
        if(str.length()>1){
            int ml = (int)str.length()/2;
            if(str.length() % 2==0)
                m= str.substring(ml-1, ml+1);
            else
                m= str.substring(ml, ml+1);
        }
        return m;
    }
}
