import java.util.Scanner;

public class ex1_1i{
    // silnia
    public static double factorial(double n){
        if(n<1)
            return 1;
        else
            return n*factorial(n-1);
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the lenght of the array: ");
        int n = input.nextInt();
        double[] sequence = new double[n];
        for (int i = 0; i < sequence.length; i++){
                System.out.print("Please enter number: ");
                sequence[i] = input.nextDouble();
        }

        double result = 0;
        for(int i=0; i<n;i++){
            result += (-1)*(sequence[i]*(Math.pow(-1,i+1)))/(factorial(i+1));
        }
        System.out.println(result);
     }
}
