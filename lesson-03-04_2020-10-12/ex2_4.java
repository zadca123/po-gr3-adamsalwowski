import java.util.Scanner;

public class ex2_4{
     public static void main(String[] args) {
         Scanner input = new Scanner(System.in);
         System.out.print("Enter the lenght of the array: ");
         int n = input.nextInt();
         double[] sequence = new double[n];
         for (int i = 0; i < sequence.length; i++){
             System.out.print("Please enter number: ");
             sequence[i] = input.nextDouble();
         }

         double max = sequence[0];
         double min = sequence[0];
         for(int i=0; i<n;i++){
             if(sequence[i]>max)
                 max = sequence[i];
             if(sequence[i]<min)
                 min = sequence[i];
         }
         System.out.println("Maximum: " + max + "\nMinimum: " + min);
     }
}
