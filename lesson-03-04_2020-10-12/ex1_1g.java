import java.util.Scanner;

public class ex1_1g{
     public static void main(String[] args) {
         Scanner input = new Scanner(System.in);
         System.out.print("Enter the lenght of the array: ");
         int n = input.nextInt();
         double[] sequence = new double[n];
         for (int i = 0; i < sequence.length; i++){
                 System.out.print("Please enter number: ");
                 sequence[i] = input.nextDouble();
         }

         double result_1 = 0;
         double result_2 = 1;
         for(int i=0; i<n;i++){
             result_1 += sequence[i];
             result_2 *= sequence[i];
         }
         System.out.println("Sum: "+ result_1 + "\nRatio: " + result_2);
     }
}
