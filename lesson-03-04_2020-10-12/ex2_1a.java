import java.util.Scanner;

public class ex2_1a{
     public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the lenght of the array: ");
        int n = input.nextInt();
        int[] sequence = new int[n];
        for (int i = 0; i < sequence.length; i++){
                System.out.print("Please enter number: ");
                sequence[i] = input.nextInt();
        }

        int result = 0;
        for(int i=0; i<n;i++){
                if(sequence[i] % 2 == 1)
                        result ++;
        }
        System.out.println("Amout of not even numbers is: " + result);
     }
}
