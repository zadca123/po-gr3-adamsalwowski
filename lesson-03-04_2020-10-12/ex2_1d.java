import java.util.Scanner;

public class ex2_1d{
        public static void main(String[] args) {
                Scanner input = new Scanner(System.in);
                System.out.print("Enter the lenght of the array: ");
                int n = input.nextInt();
                int[] sequence = new int[n];
                for (int i = 0; i < sequence.length; i++){
                        System.out.print("Please enter number: ");
                        sequence[i] = input.nextInt();
                }
                int result = 0;
                for(int i=1;i<n-1;i++)
                        if(sequence[i] < (sequence[i-1]+sequence[i+1])/2){
                                System.out.println(sequence[i] + " < (" + sequence[i-1] + " + " + sequence[i+1] + ")/2" );
                                result++;
                        }
                System.out.println("Amount of number like these is: " + result);
        }
}
