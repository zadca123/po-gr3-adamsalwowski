import java.util.Scanner;

public class ex1_1a{
     public static void main(String[] args) {
         Scanner input = new Scanner(System.in);
         System.out.print("Enter the lenght of the array: ");
         int n = input.nextInt();
         double[] sequence = new double[n];
         double result = sequence[0];

         for (int i = 0; i < sequence.length; i++){
                 System.out.print("Please enter number: ");
                 sequence[i] = input.nextDouble();
         }
         for(int i=0; i<n;i++){
             result = result + sequence[i];
         }
         System.out.println("Sum of array is: " + result);
     }
}
