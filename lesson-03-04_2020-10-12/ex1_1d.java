import java.util.Scanner;

public class ex1_1d{
     public static void main(String[] args) {
         Scanner input = new Scanner(System.in);
         System.out.print("Enter the lenght of the array: ");
         int n = input.nextInt();
         double[] sequence = new double[n];
         for (int i = 0; i < sequence.length; i++){
                 System.out.print("Please enter number: ");
                 sequence[i] = input.nextDouble();
         }

         double result = 0;
         for(int i=0; i<n;i++){
             result += Math.sqrt(Math.abs(sequence[i]));
         }
         System.out.println(result);
     }
}
