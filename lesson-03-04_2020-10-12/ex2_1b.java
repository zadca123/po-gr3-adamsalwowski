import java.util.Scanner;

public class ex2_1b{
     public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the lenght of the array: ");
        int n = input.nextInt();
        int[] sequence = new int[n];
        for (int i = 0; i < sequence.length; i++){
                System.out.print("Please enter number: ");
                sequence[i] = input.nextInt();
        }

        int result = 0;
        for(int i=0; i<n;i++){
            int mod3 = sequence[i] % 3;
            int mod5 = sequence[i] % 5;
            if(mod3 == 0 & mod5 > 0)
                result++;
        }
        System.out.println("Amount of numbers that are divisible by 3 but not 5 is: " + result);
     }
}
