import java.util.Scanner;

public class ex2_3{
        public static void main(String[] args){
                Scanner input = new Scanner(System.in);
                System.out.print("Enter the lenght of the array: ");
                int n = input.nextInt();
                double[] sequence = new double[n];
                for (int i = 0; i < sequence.length; i++){
                        System.out.print("Please enter number: ");
                        sequence[i] = input.nextDouble();
                }

                int positive = 0;
                int negative = 0;
                int zeroes = 0;
                for(int i=0; i<n;i++){
                        if(sequence[i]>0)
                                positive++;
                        else if(sequence[i]<0)
                                negative++;
                        else
                                zeroes++;
                }
                System.out.println("Amount of positive numbers is: " + positive);
                System.out.println("Amount of negative numbers is: " + negative);
                System.out.println("Amount of zeroes : " + zeroes);
        }
}
