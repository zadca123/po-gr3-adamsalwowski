import java.util.Scanner;

public class ex2_1e{
    public static int factorial(int n){
        if(n<1)
            return 1;
        else
            return n*factorial(n-1);
    }
     public static void main(String[] args) {
         Scanner input = new Scanner(System.in);
         System.out.print("Enter the lenght of the array: ");
         int n = input.nextInt();
         int[] sequence = new int[n];
         for (int i = 0; i < sequence.length; i++){
             System.out.print("Please enter number: ");
             sequence[i] = input.nextInt();
         }

         int result = 0;
         for(int i=1;i<n;i++)
             if(Math.pow(2,i) < sequence[i] & sequence[i]< factorial(i) ){
                 System.out.println(Math.pow(2,i) + " < " + sequence[i] + " < " + i + "!=" + factorial(i));
                 result++;
             }
         System.out.println("Amount of number like these is: " + result);
     }
}
