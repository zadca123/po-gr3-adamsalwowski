import java.io.*;
import java.util.ArrayList;

public class ex4{
    public static void main(String[] args){
        int len = 8;
        ArrayList<Integer> arr1 = new ArrayList<Integer>(len);
        // ArrayList<Integer> arr2 = new ArrayList<Integer>(len);
        for (int i = 0; i < len; i++) {
            arr1.add(i, arr1.size()+i+1);
            // arr2.add(i, arr2.size()*i*i);
        }
        System.out.println(arr1);

        ArrayList<Integer> arr3 = reversed(arr1);
        System.out.println(arr3);
    }
    public static ArrayList<Integer> reversed(ArrayList<Integer> arr1){
        ArrayList<Integer> result = new ArrayList<Integer>();
        for(int i=0; i < arr1.size(); i++)
            result.add(i, arr1.get(arr1.size()-i-1));
        return result;
    }
}
