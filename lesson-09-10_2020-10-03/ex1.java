import java.io.*;
import java.util.ArrayList;

public class ex1{
    public static void main(String[] args){
        int len = 8;
        ArrayList<Integer> arr1 = new ArrayList<Integer>(len);
        ArrayList<Integer> arr2 = new ArrayList<Integer>(len);
        for (int i = 0; i < len; i++) {
            arr1.add(i, arr1.size()+i+1);
            arr2.add(i, arr2.size()*i*i);
        }
        System.out.println(arr1);
        System.out.println(arr2);

        ArrayList<Integer> arr3 = append(arr1, arr2);
        System.out.println(arr3);
    }
    public static ArrayList<Integer> append(ArrayList<Integer> arr1, ArrayList<Integer> arr2){
        ArrayList<Integer> arrL = new ArrayList<Integer>();
        arrL.addAll(0, arr1);
        arrL.addAll(arr1.size(), arr2);
        return arrL;
    }
}
