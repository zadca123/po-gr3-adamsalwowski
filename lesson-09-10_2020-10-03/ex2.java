import java.io.*;
import java.util.ArrayList;

public class ex2{
    public static void main(String[] args){
        int len = 8;
        ArrayList<Integer> arr1 = new ArrayList<Integer>(len);
        ArrayList<Integer> arr2 = new ArrayList<Integer>(len);
        for (int i = 0; i < len; i++) {
            arr1.add(i, 1);
            arr2.add(i, 0);
        }
        arr2.add(arr2.size(), 0);
        arr2.add(arr2.size(), 0);
        arr2.add(arr2.size(), 0);
        arr2.add(arr2.size(), 0);
        arr2.add(arr2.size(), 0);
        arr2.add(arr2.size(), 0);
        arr2.add(arr2.size(), 0);
        arr2.add(arr2.size(), 0);
        arr2.add(arr2.size(), 0);

        System.out.println(arr1);
        System.out.println(arr2);
        // System.out.println(arr2[2]);
        ArrayList<Integer> arr3 = merge(arr1, arr2);
        System.out.println(arr3);
    }
    public static ArrayList<Integer> merge(ArrayList<Integer> arr1, ArrayList<Integer> arr2){
        ArrayList<Integer> result = new ArrayList<Integer>();
        int minlen = arr1.size();
        int check = 0;
        if(arr2.size() < minlen){
            minlen = arr2.size();
            check = 1;
        }
        int temp;
        for (int i = 0; i < minlen; i++) {
            result.add(result.size(), arr1.get(i));
            result.add(result.size(), arr2.get(i));
            if(i == minlen-1)
                if(check == 0)
                    for(int j = minlen; j< arr2.size(); j++)
                        result.add(result.size(), arr2.get(j));
                else
                    for(int j = minlen; j< arr1.size(); j++)
                        result.add(result.size(), arr1.get(j));
        }

        return result;
    }
}
