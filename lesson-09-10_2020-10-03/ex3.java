import java.io.*;
import java.util.ArrayList;

public class ex3{
    public static void main(String[] args){
        int len = 8;
        ArrayList<Integer> arr1 = new ArrayList<Integer>(len);
        ArrayList<Integer> arr2 = new ArrayList<Integer>(len);
        for (int i = 0; i < len; i++) {
            arr1.add(i, arr1.size()+i+1);
            arr2.add(i, arr2.size()*i*i);
        }
        System.out.println(arr1);
        System.out.println(arr2);

        ArrayList<Integer> arr3 = mergeSorted(arr1, arr2);
        System.out.println(arr3);
    }
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> arr1, ArrayList<Integer> arr2){
        ArrayList<Integer> arrL = new ArrayList<Integer>();
        arrL.addAll(0, arr1);
        arrL.addAll(arr1.size(), arr2);
        int n = arrL.size();
        int temp;
        for (int x = 0; x<n; x++)
                for(int i=1; i < n; i++)
                        if(arrL.get(i-1) > arrL.get(i)){
                                temp = arrL.get(i-1);
                                arrL.set(i-1, arrL.get(i));
                                arrL.set(i, temp);
                        }
        return arrL;
    }
}
