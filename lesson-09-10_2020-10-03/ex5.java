import java.io.*;
import java.util.ArrayList;

public class ex5{
    public static void main(String[] args){
        int len = 7;
        ArrayList<Integer> arr1 = new ArrayList<Integer>(len);
        // ArrayList<Integer> arr2 = new ArrayList<Integer>(len);
        for (int i = 0; i < len; i++) {
            arr1.add(i, arr1.size()+i+1);
            // arr2.add(i, arr2.size()*i*i);
        }
        System.out.println(arr1);

        reverse(arr1);
        System.out.println(arr1);
    }
    public static void reverse(ArrayList<Integer> arr1){
        int temp;
        for(int i=0; i < arr1.size()/2; i++){
            temp = arr1.get(arr1.size()-i-1);
            arr1.set(arr1.size()-i-1, arr1.get(i));
            arr1.set(i, temp);
        }
    }
}
