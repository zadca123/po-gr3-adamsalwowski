import java.util.*;

public class ex2{
    public static void main(String[] args){
        String char_array= "chillcat";
        char todelete = 'c';
        System.out.println(delchar(char_array, todelete));
    }
    public static String delchar(String str, char toDel){
        char[] characters = str.toCharArray();
        char[] result = new char[str.length()];
        int count = 0;
        for (int i = 1; i < characters.length; i++) {
            if(characters[i-1] == toDel)
                count++;
            else if(count >=1)
                result[i-1] = characters[i];
            else
                result[i] = characters[i];
        }
        return result.toString();
    }
}
