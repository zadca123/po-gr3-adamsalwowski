package pl.imiajd.Salwowski;
import java.util.*;
import java.util.Map.*;
import java.time.*;

public class Laptop extends Komputer implements Cloneable, Comparable<Komputer>{
    public Laptop(String nazwa, LocalDate dataProdukcji, boolean czyApple){
        super(nazwa,dataProdukcji);
        this.czyApple = czyApple;
    }
    private boolean czyApple;

    public String toString() {
        return "Nazwa: "+ super.getNazwa() + "\tData Produkcji: " +
            super.getDataProdukcji() + "\tCzy Apple: " + czyApple;
    }
    public boolean getCzyApple(){
        return czyApple;
    }

    public int compareTo(Laptop obj){
        if(super.compareTo(obj) == 0)
            return Boolean.toString(czyApple).compareTo(Boolean.toString(obj.getCzyApple()));
        return super.compareTo(obj);
    }
}
