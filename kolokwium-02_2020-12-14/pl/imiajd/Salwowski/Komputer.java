package pl.imiajd.Salwowski;
import java.util.*;
import java.util.Map.*;
import java.time.*;

public class Komputer implements Cloneable, Comparable<Komputer>{
    public Komputer(String nazwa, LocalDate dataProdukcji){
        this.nazwa = nazwa;
        this.dataProdukcji = dataProdukcji;
    }
    private String nazwa;
    private LocalDate dataProdukcji;

    public String getNazwa(){
        return nazwa;
    }
    public LocalDate getDataProdukcji(){
        return dataProdukcji;
    }
    public String toString() {
        return "Nazwa: "+ nazwa + "\tData Produkcji: " + dataProdukcji;
    }

    @Override
    public Komputer clone() throws CloneNotSupportedException{
        return clone();
    }

    public int compareTo(Komputer obj){
        if(nazwa.compareTo(obj.getNazwa()) == 0)
            return dataProdukcji.compareTo(obj.getDataProdukcji());
        return nazwa.compareTo(obj.getNazwa());
    }
    public boolean equals(Komputer obj){
        if(this == obj)
            return true;
        if( obj == null || getClass() != obj.getClass())
            return false;
        Komputer k = (Komputer) obj;
        return Objects.equals(nazwa, k.getNazwa()) && Objects.equals(dataProdukcji, k.getDataProdukcji());
    }
}
