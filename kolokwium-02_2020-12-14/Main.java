import pl.imiajd.Salwowski.Komputer;
import pl.imiajd.Salwowski.Laptop;
import java.util.*;
import java.util.Map.*;
import java.time.*;

public class Main {
    public static void main(String[] args) {
        ArrayList<Komputer> grupa = new ArrayList<>();

        grupa.add(new Komputer("IBM 2500",LocalDate.of(1977,1,10)));
        grupa.add(new Komputer("IBM 2500",LocalDate.of(1977,2,10)));
        grupa.add(new Komputer("IBM 2502",LocalDate.of(1937,3,10)));
        grupa.add(new Komputer("IBM 2503",LocalDate.of(1937,3,10)));
        grupa.add(new Komputer("IBM 2504",LocalDate.of(1927,5,10)));

        for (Komputer var : grupa)
                System.out.println(var.toString());
        grupa.sort(Komputer::compareTo);
        System.out.println("-----Sorted----- ");
        for (Komputer var : grupa)
                System.out.println(var.toString());



        ArrayList<Laptop> grupaLaptopow = new ArrayList<>();
        grupaLaptopow.add(new Laptop("IBM 2500", LocalDate.of(1977,1,10), false));
        grupaLaptopow.add(new Laptop("IBM 2500", LocalDate.of(1977,2,10), false));
        grupaLaptopow.add(new Laptop("IBM 2502", LocalDate.of(1937,3,10), false));
        grupaLaptopow.add(new Laptop("IBM 2503", LocalDate.of(1937,3,10), false));
        grupaLaptopow.add(new Laptop("Macintosh", LocalDate.of(1927,5,10), true));

        for (Laptop var : grupaLaptopow)
                System.out.println(var.toString());
        grupaLaptopow.sort(Laptop::compareTo);
        System.out.println("-----Sorted----- ");
        for (Laptop var : grupaLaptopow)
                System.out.println(var.toString());
    }
}
