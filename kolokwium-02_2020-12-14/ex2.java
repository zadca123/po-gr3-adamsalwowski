import java.util.*;
import java.util.Map.*;
import java.time.*;
import pl.imiajd.Salwowski.*;

public class ex2 {
    public static void main(String[] args) {
        LinkedList<Komputer> temp = new LinkedList<>();
        temp.add(new Komputer("IBM 2500",LocalDate.of(1977,1,10)));
        temp.add(new Komputer("IBM 2500",LocalDate.of(1977,2,10)));
        temp.add(new Komputer("IBM 2502",LocalDate.of(1937,3,10)));
        temp.add(new Komputer("IBM 2503",LocalDate.of(1937,3,10)));
        temp.add(new Komputer("IBM 2504",LocalDate.of(1927,5,10)));
        LinkedList<String> komputery = new LinkedList<>();
        for (Komputer var : temp) {
            komputery.add(var.toString());
        }


        for (String var : komputery)
            System.out.println(var);
        redukuj(komputery,2);
        System.out.println();
        for (String var : komputery)
            System.out.println(var);
    }

    public static void redukuj(LinkedList<String> linkList, int n){
        for (int i = 0; i < linkList.size(); i+=n-1)
            linkList.remove(i);
    }
}
