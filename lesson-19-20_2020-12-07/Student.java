public static class Student implements Comparable<Student>{
    public Student(String name, String surname,int id) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }
    private String name;
    private String surname;
    private int id;

    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public int getId() {
        return id;
    }

    public int compareTo(Student obj){
        if(surname.compareTo(obj.getSurname()) == 0){
            if(name.compareTo(obj.getName()) == 0){
                return Integer.compare(id, obj.getId());
            }
            return name.compareTo(obj.getName());
        }
        return surname.compareTo(obj.getSurname());
    }
}
