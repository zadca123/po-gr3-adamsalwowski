import java.util.PriorityQueue;

public class ToDoList implements Comparable<ToDoList>{
    public ToDoList(int priority, String description){
        this.priority = priority;
        this.description = description;
    }
    public PriorityQueue<ToDoList> theList = new PriorityQueue<ToDoList>();
    private int priority;
    private String description;
    // if(this.priority>10 || this.priority<0)
    //     this.priority = 9;

    public int compareTo(ToDoList obj){
        int a = (this.description.compareTo(obj.getDescription()));
        int b = (Integer.compare(this.priority, obj.getPriority()));
            // (this.priority.compareTo(obj.getPriority()));
        if(a<0 && b<0)
            return a*b *(-1);
        return a*b;
    }
    public String toString(){
        return "Priority: " + priority + "\tDescription: " + description;
    }

    // setTask
    public void addTask(int priority, String description){
        this.priority = priority;
        this.description = description;
    }
    public int getPriority(){
        return priority;
    }
    public String getDescription(){
        return description;
    }
    public void setPriority(int priority){
        if(priority > 0 || priority<10)
            this.priority = priority;
        System.out.println("Priority must be a integer beetween 1 to 9");
    }
    public void setDescription(String description){
        this.description = description;
    }

    // public void manage(){
    //     boolean x = true;
    //     Scanner scan = new Scanner(System.in);
    //     String val0 = new String();
    //     String val1 = new String();
    //     String val2 = new String();
    //     do{
    //         System.out.print("\nWhat you want to do? (Enter the coresponding number)\n" +
    //                          "1. Add\n2. Delete\n3. Change Grade\n4. WriteOut\n5. Exit\n\nPrompt: ");
    //         val0 = scan.nextLine();
    //         switch(val0){
    //         case "1":
    //             System.out.print("Enter Surname: ");
    //             val1 = scan.nextLine();
    //             System.out.print("Enter Value/Grade: ");
    //             val2 = scan.nextLine();
    //             add(val1,val2);
    //             break;
    //         case "2":
    //             System.out.print("Enter Surname to delete: ");
    //             val1 = scan.nextLine();
    //             del(val1);
    //             break;
    //         case "3":
    //             System.out.print("Enter Surname: ");
    //             val1 = scan.nextLine();
    //             System.out.print("Enter new Value/Grade: ");
    //             val2 = scan.nextLine();
    //             changeGrade(val1,val2);
    //             break;
    //         case "4":
    //             writeOut();
    //             break;
    //         case "5":
    //             x = false;
    //             break;
    //         default:
    //             System.out.println("Wrong input");
    //             break;
    //         }
    //     }while(x == true);
    // }
}
